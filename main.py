class Human():
    def __init__(self, year = 2022, name = 'Ivan', age = 23, hair = 'black wavy hair'):
        self.year = year
        self.name = name
        self.age = age
        self.hair = hair

    def next_year(self):
        self.year += 1

    def new_name(self):
        self.name = 'Daniil'

    def plus_age(self):
        self.age += 1

    def new_hair(self):
        self.hair = 'blond straight hair'

class Kirill(Human):
    def __init__(self):
        super().__init__()
        self.ear_movement = False
    
    def ear_ther_is(self):
        self.ear_movement = True

    def ear_ther_is_not(self):
        self.ear_movement = False

human = Kirill()
human.next_year()
human.new_name()
human.plus_age()    
human.new_hair()
human.ear_ther_is()
print(human.year)    
print(human.name)
print(human.age)
print(human.hair)
print(human.ear_movement)